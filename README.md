<div style="text-align:center"><img src="Integration-Stuff/icons/128x128/voicegen.png" ></img></div>

# VoiceGen

VoiceGen is a simple but powerful text to speech appplication, with support for multiple offline/online engines such as svox and Amazon Polly.



# Installation

## AUR
easily install it on Arch, Manjaro, Endeavour or any arch based distro.

`yay -S voicegen`

## Flatpak
[![Get it from Flathub](https://flathub.org/assets/badges/flathub-badge-en.png)](https://flathub.org/apps/details/io.gitlab.persiangolf.voicegen)


# Build instructions

## Ubuntu/Pop! OS/Mint/MX linux
**For building**: `sudo apt install cmake qtbase5-dev qtmultimedia5-dev`

**For running/installing**: `sudo apt install qtbase5 libqt5mutimedia5 libqt5multimedia5-plugins`

**Optional** (if you want svox pico engine) `sudo apt install libttspico-utils ffmpeg`



You need Qt5 and CMake 2.8.11 or higher:

`cd voicegen`

`mkdir build`

`cd build`

`cmake ..`

`make`

`./voicegen`


# Buy me a Coffee ☕

**Monero:**
47ME2ogosZGNJV89SaBPiSNkqExVwbpK5UBb7Kp73WXmgJxXsQdniGtBD5VRf48rZH6UksNCntRoP6XvHGeW1mBs1rFW7s1

**Etherium:**
0xd8F3200BF4728D77E2F26448caE97c8e132e71C1
