/****************************************************************************
** Meta object code from reading C++ file 'voicegen.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../../src/voicegen.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'voicegen.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_VoiceGen_t {
    QByteArrayData data[11];
    char stringdata0[224];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_VoiceGen_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_VoiceGen_t qt_meta_stringdata_VoiceGen = {
    {
QT_MOC_LITERAL(0, 0, 8), // "VoiceGen"
QT_MOC_LITERAL(1, 9, 23), // "on_actionRead_triggered"
QT_MOC_LITERAL(2, 33, 0), // ""
QT_MOC_LITERAL(3, 34, 24), // "on_actionAbout_triggered"
QT_MOC_LITERAL(4, 59, 23), // "on_actionStop_triggered"
QT_MOC_LITERAL(5, 83, 23), // "on_actionQuit_triggered"
QT_MOC_LITERAL(6, 107, 37), // "on_engineCombobox_currentInde..."
QT_MOC_LITERAL(7, 145, 26), // "on_actionSave_As_triggered"
QT_MOC_LITERAL(8, 172, 23), // "on_actionSave_triggered"
QT_MOC_LITERAL(9, 196, 21), // "on_ttsthread_finished"
QT_MOC_LITERAL(10, 218, 5) // "error"

    },
    "VoiceGen\0on_actionRead_triggered\0\0"
    "on_actionAbout_triggered\0"
    "on_actionStop_triggered\0on_actionQuit_triggered\0"
    "on_engineCombobox_currentIndexChanged\0"
    "on_actionSave_As_triggered\0"
    "on_actionSave_triggered\0on_ttsthread_finished\0"
    "error"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_VoiceGen[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   54,    2, 0x08 /* Private */,
       3,    0,   55,    2, 0x08 /* Private */,
       4,    0,   56,    2, 0x08 /* Private */,
       5,    0,   57,    2, 0x08 /* Private */,
       6,    0,   58,    2, 0x08 /* Private */,
       7,    0,   59,    2, 0x08 /* Private */,
       8,    0,   60,    2, 0x08 /* Private */,
       9,    1,   61,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   10,

       0        // eod
};

void VoiceGen::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<VoiceGen *>(_o);
        (void)_t;
        switch (_id) {
        case 0: _t->on_actionRead_triggered(); break;
        case 1: _t->on_actionAbout_triggered(); break;
        case 2: _t->on_actionStop_triggered(); break;
        case 3: _t->on_actionQuit_triggered(); break;
        case 4: _t->on_engineCombobox_currentIndexChanged(); break;
        case 5: _t->on_actionSave_As_triggered(); break;
        case 6: _t->on_actionSave_triggered(); break;
        case 7: _t->on_ttsthread_finished((*reinterpret_cast< QString(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject VoiceGen::staticMetaObject = { {
    QMetaObject::SuperData::link<QMainWindow::staticMetaObject>(),
    qt_meta_stringdata_VoiceGen.data,
    qt_meta_data_VoiceGen,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *VoiceGen::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *VoiceGen::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_VoiceGen.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int VoiceGen::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 8;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
