/********************************************************************************
** Form generated from reading UI file 'voicegen.ui'
**
** Created by: Qt User Interface Compiler version 5.15.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_VOICEGEN_H
#define UI_VOICEGEN_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_VoiceGen
{
public:
    QAction *actionQuit;
    QAction *actionAbout;
    QAction *actionClear_Text_Box;
    QAction *actionRead;
    QAction *actionSave;
    QAction *actionSave_As;
    QAction *actionStop;
    QAction *actionPaste;
    QAction *actionToggle_MenuBar;
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QPlainTextEdit *main_input;
    QHBoxLayout *ControlLayout;
    QComboBox *engineCombobox;
    QComboBox *voiceListCombobox;
    QSpacerItem *horizontalSpacer;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuAbout;
    QMenu *menuEdit;
    QMenu *menuGo;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *VoiceGen)
    {
        if (VoiceGen->objectName().isEmpty())
            VoiceGen->setObjectName(QString::fromUtf8("VoiceGen"));
        VoiceGen->resize(915, 534);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/mainresource/voicegenicon.png"), QSize(), QIcon::Normal, QIcon::Off);
        VoiceGen->setWindowIcon(icon);
        actionQuit = new QAction(VoiceGen);
        actionQuit->setObjectName(QString::fromUtf8("actionQuit"));
        QIcon icon1;
        QString iconThemeName = QString::fromUtf8("application-exit");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon1 = QIcon::fromTheme(iconThemeName);
        } else {
            icon1.addFile(QString::fromUtf8("../../../.designer/backup"), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionQuit->setIcon(icon1);
        actionAbout = new QAction(VoiceGen);
        actionAbout->setObjectName(QString::fromUtf8("actionAbout"));
        actionAbout->setIcon(icon);
        actionClear_Text_Box = new QAction(VoiceGen);
        actionClear_Text_Box->setObjectName(QString::fromUtf8("actionClear_Text_Box"));
        actionClear_Text_Box->setCheckable(false);
        QIcon icon2;
        iconThemeName = QString::fromUtf8("edit-clear");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon2 = QIcon::fromTheme(iconThemeName);
        } else {
            icon2.addFile(QString::fromUtf8("../../../.designer/backup"), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionClear_Text_Box->setIcon(icon2);
        actionRead = new QAction(VoiceGen);
        actionRead->setObjectName(QString::fromUtf8("actionRead"));
        QIcon icon3;
        iconThemeName = QString::fromUtf8("text-speak");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon3 = QIcon::fromTheme(iconThemeName);
        } else {
            icon3.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionRead->setIcon(icon3);
        actionSave = new QAction(VoiceGen);
        actionSave->setObjectName(QString::fromUtf8("actionSave"));
        QIcon icon4;
        iconThemeName = QString::fromUtf8("document-save");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon4 = QIcon::fromTheme(iconThemeName);
        } else {
            icon4.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionSave->setIcon(icon4);
        actionSave_As = new QAction(VoiceGen);
        actionSave_As->setObjectName(QString::fromUtf8("actionSave_As"));
        QIcon icon5;
        iconThemeName = QString::fromUtf8("document-save-as");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon5 = QIcon::fromTheme(iconThemeName);
        } else {
            icon5.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionSave_As->setIcon(icon5);
        actionStop = new QAction(VoiceGen);
        actionStop->setObjectName(QString::fromUtf8("actionStop"));
        QIcon icon6;
        iconThemeName = QString::fromUtf8("process-stop");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon6 = QIcon::fromTheme(iconThemeName);
        } else {
            icon6.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionStop->setIcon(icon6);
        actionPaste = new QAction(VoiceGen);
        actionPaste->setObjectName(QString::fromUtf8("actionPaste"));
        QIcon icon7(QIcon::fromTheme(QString::fromUtf8("edit-paste")));
        actionPaste->setIcon(icon7);
        actionToggle_MenuBar = new QAction(VoiceGen);
        actionToggle_MenuBar->setObjectName(QString::fromUtf8("actionToggle_MenuBar"));
        centralWidget = new QWidget(VoiceGen);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        main_input = new QPlainTextEdit(centralWidget);
        main_input->setObjectName(QString::fromUtf8("main_input"));
        QFont font;
        font.setPointSize(12);
        font.setBold(false);
        font.setWeight(50);
        main_input->setFont(font);

        verticalLayout->addWidget(main_input);

        ControlLayout = new QHBoxLayout();
        ControlLayout->setObjectName(QString::fromUtf8("ControlLayout"));
        engineCombobox = new QComboBox(centralWidget);
        engineCombobox->addItem(QString());
        engineCombobox->addItem(QString());
        engineCombobox->addItem(QString());
        engineCombobox->setObjectName(QString::fromUtf8("engineCombobox"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(engineCombobox->sizePolicy().hasHeightForWidth());
        engineCombobox->setSizePolicy(sizePolicy);
        engineCombobox->setMinimumSize(QSize(132, 0));

        ControlLayout->addWidget(engineCombobox);

        voiceListCombobox = new QComboBox(centralWidget);
        voiceListCombobox->setObjectName(QString::fromUtf8("voiceListCombobox"));
        voiceListCombobox->setEnabled(true);
        voiceListCombobox->setMinimumSize(QSize(110, 0));
        voiceListCombobox->setMouseTracking(false);

        ControlLayout->addWidget(voiceListCombobox);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::MinimumExpanding, QSizePolicy::Minimum);

        ControlLayout->addItem(horizontalSpacer);


        verticalLayout->addLayout(ControlLayout);

        VoiceGen->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(VoiceGen);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 915, 32));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QString::fromUtf8("menuFile"));
        menuAbout = new QMenu(menuBar);
        menuAbout->setObjectName(QString::fromUtf8("menuAbout"));
        menuEdit = new QMenu(menuBar);
        menuEdit->setObjectName(QString::fromUtf8("menuEdit"));
        menuGo = new QMenu(menuBar);
        menuGo->setObjectName(QString::fromUtf8("menuGo"));
        VoiceGen->setMenuBar(menuBar);
        mainToolBar = new QToolBar(VoiceGen);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        mainToolBar->setMovable(true);
        mainToolBar->setOrientation(Qt::Horizontal);
        mainToolBar->setToolButtonStyle(Qt::ToolButtonFollowStyle);
        mainToolBar->setFloatable(true);
        VoiceGen->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(VoiceGen);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        statusBar->setEnabled(true);
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(statusBar->sizePolicy().hasHeightForWidth());
        statusBar->setSizePolicy(sizePolicy1);
        statusBar->setLayoutDirection(Qt::LeftToRight);
        statusBar->setSizeGripEnabled(false);
        VoiceGen->setStatusBar(statusBar);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuGo->menuAction());
        menuBar->addAction(menuEdit->menuAction());
        menuBar->addAction(menuAbout->menuAction());
        menuFile->addAction(actionSave);
        menuFile->addAction(actionSave_As);
        menuFile->addAction(actionQuit);
        menuAbout->addAction(actionAbout);
        menuEdit->addAction(actionClear_Text_Box);
        menuEdit->addAction(actionPaste);
        menuEdit->addSeparator();
        menuGo->addAction(actionRead);
        menuGo->addAction(actionStop);
        menuGo->addSeparator();
        mainToolBar->addAction(actionRead);
        mainToolBar->addAction(actionStop);
        mainToolBar->addSeparator();
        mainToolBar->addAction(actionPaste);
        mainToolBar->addAction(actionClear_Text_Box);
        mainToolBar->addAction(actionSave_As);
        mainToolBar->addAction(actionSave);
        mainToolBar->addSeparator();
        mainToolBar->addAction(actionAbout);

        retranslateUi(VoiceGen);
        QObject::connect(actionClear_Text_Box, SIGNAL(triggered()), main_input, SLOT(clear()));
        QObject::connect(actionPaste, SIGNAL(triggered()), main_input, SLOT(paste()));

        engineCombobox->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(VoiceGen);
    } // setupUi

    void retranslateUi(QMainWindow *VoiceGen)
    {
        VoiceGen->setWindowTitle(QCoreApplication::translate("VoiceGen", "VoiceGen (Beta)", nullptr));
        actionQuit->setText(QCoreApplication::translate("VoiceGen", "Quit", nullptr));
        actionAbout->setText(QCoreApplication::translate("VoiceGen", "About VoiceGen", nullptr));
        actionClear_Text_Box->setText(QCoreApplication::translate("VoiceGen", "Clear All", nullptr));
#if QT_CONFIG(statustip)
        actionClear_Text_Box->setStatusTip(QString());
#endif // QT_CONFIG(statustip)
        actionRead->setText(QCoreApplication::translate("VoiceGen", "Read!", nullptr));
        actionSave->setText(QCoreApplication::translate("VoiceGen", "Save", nullptr));
        actionSave_As->setText(QCoreApplication::translate("VoiceGen", "Save As", nullptr));
        actionStop->setText(QCoreApplication::translate("VoiceGen", "Stop", nullptr));
        actionPaste->setText(QCoreApplication::translate("VoiceGen", "Paste", nullptr));
        actionToggle_MenuBar->setText(QCoreApplication::translate("VoiceGen", "Toggle MenuBar", nullptr));
#if QT_CONFIG(shortcut)
        actionToggle_MenuBar->setShortcut(QCoreApplication::translate("VoiceGen", "Ctrl+M", nullptr));
#endif // QT_CONFIG(shortcut)
#if QT_CONFIG(tooltip)
        main_input->setToolTip(QString());
#endif // QT_CONFIG(tooltip)
        main_input->setPlaceholderText(QCoreApplication::translate("VoiceGen", "Type Your Text Here", nullptr));
        engineCombobox->setItemText(0, QCoreApplication::translate("VoiceGen", "PicoTTS (Native)", nullptr));
        engineCombobox->setItemText(1, QCoreApplication::translate("VoiceGen", "StreamLabs", nullptr));
        engineCombobox->setItemText(2, QCoreApplication::translate("VoiceGen", "Google", nullptr));

        menuFile->setTitle(QCoreApplication::translate("VoiceGen", "File", nullptr));
        menuAbout->setTitle(QCoreApplication::translate("VoiceGen", "Help", nullptr));
        menuEdit->setTitle(QCoreApplication::translate("VoiceGen", "Edit", nullptr));
        menuGo->setTitle(QCoreApplication::translate("VoiceGen", "Go", nullptr));
    } // retranslateUi

};

namespace Ui {
    class VoiceGen: public Ui_VoiceGen {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_VOICEGEN_H
