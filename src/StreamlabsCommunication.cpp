#include "StreamlabsCommunication.h"

QString StreamlabsCommunication::commandReadString(const QString &cmd) //with this function we actually can communicate to streamlabs servers using wget
{
    char buffer[128];
    QString outputstr;
    FILE* pipe = popen(cmd.toStdString().c_str(), "r");
    while (fgets(buffer, sizeof buffer, pipe) != NULL)
    {
        outputstr += buffer;
    }
    pclose(pipe);
    return outputstr;
}



StreamlabsCommunication::StreamlabsCommunication(const QString &Voice, QString InputText)
{
    //preparing the string for the linux command line
    InputText.replace('\\',"\'\\\\\'");//   '/'
    InputText.replace('"',"\'\\\"\'");//    '\"'
    InputText.replace('\'',"\'\\\'\'");//   '\''


    QString jsonstring = "{ \"voice\": \"" + Voice + "\", \"text\": \"" + InputText + "\" }";
    QString datacommand = "CURL_DATA=\'" + jsonstring + "\'";
    QString curl_command = "curl --silent -X POST --data \"$CURL_DATA\" -H 'Content-Type: application/json' 'https://streamlabs.com/polly/speak'";

    serverResponse=commandReadString(datacommand +" && "+ curl_command);
    SuccessCheck = !(serverResponse.contains("{\"error\":") || serverResponse.length()==0);
}


QString StreamlabsCommunication::parseLink()//parses the json responce
{
    if (SuccessCheck==true)
    {
        serverResponse.remove(0,29);//See the line 53
        serverResponse.replace("\\","");//remove backslashes
        serverResponse.remove(serverResponse.length()-2,2);// removes ' "} ' at the end of string
    }
    return serverResponse;
}


bool StreamlabsCommunication::isSuccessful()
{
    return SuccessCheck;
}


/*output is somthing like this:

{"success":true,"speak_url":"https:\/\/polly.streamlabs.com\/v1\/speech?OutputFormat=mp3&Text=Hello%20There&VoiceId=Brian&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAIHKNQTJ7BGLEFVZA%2F20210923%2Fus-west-2%2Fpolly%2Faws4_request&X-Amz-Date=20210923T194146Z&X-Amz-SignedHeaders=host&X-Amz-Expires=900&X-Amz-Signature=fbeef9d5dac5bc1a97590399dc322a5fac36ecf98c139f39f20c50a62f72444f"}
*/




