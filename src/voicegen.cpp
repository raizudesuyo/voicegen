#include "voicegen.h"
#include "ui_voicegen.h"
#include <QMessageBox>
#include <QFileDialog>
#include <QFile>
#include <iostream>
#include <QDialog>
#include "TextToSpeech.h"
#include <array>

using namespace std;

VoiceGen::VoiceGen(QWidget *parent) :
    QMainWindow(parent),
    m_ui(new Ui::VoiceGen)
{
    player = new QMediaPlayer(this);
    tts = new TextToSpeech(player);

    m_ui->setupUi(this);
    m_ui->engineCombobox->setCurrentIndex(2);// I set it on Streamlabs because it doesn't need any dependancy and it just works out of the box
    this->on_engineCombobox_currentIndexChanged();
    m_ui->menuBar->hide();
    menubarShortcut = new QShortcut(QKeySequence("Ctrl+M"),this);
    connect(menubarShortcut, &QShortcut::activated, this , &VoiceGen::on_ctrlm_triggered);

    //it seems that the GNOME desktop environment does not care about icons that we set in designer so we set costom icons if the user uses GNOME DE.
    QString DesktopEnvironment = std::getenv("XDG_CURRENT_DESKTOP");
    QList<QAction *> actions = m_ui->mainToolBar->actions();
    if(DesktopEnvironment == "GNOME")
    {
        std::array<const QString,7> newicons {"read", "stop" , "", "paste", "clear", "save-as", "save"};
        for (auto a = 0; a < newicons.size(); a++)
        {
            actions[a]->setIcon(QIcon(":/mainresource/"+newicons[a]+".png"));
        }
    }
    //text-speak is not XDG-standard. so we fallback into a safe icon (available in XDG) if it is not there.
    else if(!QIcon::hasThemeIcon("text-speak"))
    {
        if (QIcon::hasThemeIcon("audio-volume-high-symbolic"))actions[0]->setIcon(QIcon::fromTheme("audio-volume-high-symbolic"));
        else actions[0]->setIcon(QIcon::fromTheme("audio-volume-high"));
    }


    if (!QFile::exists("/usr/bin/pico2wave")  && !QFile::exists("/app/bin/pico2wave"))
    {
        //this is a hack to "hide an option"
        m_ui->engineCombobox->setItemData(0, false, Qt::UserRole -1);
        m_ui->engineCombobox->setItemData(0, QSize(0,0), Qt::SizeHintRole);
        m_ui->engineCombobox->setItemText(0,"");
        m_ui->engineCombobox->setItemData(1, false, Qt::UserRole -1);
        m_ui->engineCombobox->setItemData(1, QSize(0,0), Qt::SizeHintRole);
        m_ui->engineCombobox->setItemText(1,"");

    }
    else if (!QFile::exists("/bin/ffmpeg"))
    {
        m_ui->engineCombobox->setItemData(1, false, Qt::UserRole -1);
        m_ui->engineCombobox->setItemData(1, QSize(0,0), Qt::SizeHintRole);
        m_ui->engineCombobox->setItemText(1,"");
    }

    m_ui->engineCombobox->setItemData(3, false, Qt::UserRole -1);
    m_ui->engineCombobox->setItemData(3, QSize(0,0), Qt::SizeHintRole);
    m_ui->engineCombobox->setItemText(3,"");

    connect(tts,SIGNAL(taskFinished(QString)),this,SLOT(on_ttsthread_finished(QString)));
}


VoiceGen::~VoiceGen() = default;

void VoiceGen::on_ctrlm_triggered()
{
    static bool showhideflag;
    showhideflag = !showhideflag;
    m_ui->menuBar->setVisible(showhideflag);
}



void VoiceGen::on_ttsthread_finished(QString error)
{
    if(error.isEmpty())
    {
        static QString SavePath;
        static int append;
        if (readsaveflag==read)
        {
            m_ui->statusBar->showMessage("Reading aloud", 3000);
            return;
        }

        else if (readsaveflag == saveAs)
        {
            SavePath = QFileDialog::getSaveFileName(this,"Save To","/home/");
            if (!SavePath.isEmpty())QFile::copy("/tmp/voice",SavePath);// if the user didn't hit the cancel button in SaveAs dialog
            append = 0;
        }
        else //save
        {
            if (SavePath.isEmpty())
            {
                SavePath=QFileDialog::getSaveFileName(this,"Save To","/home/");
                if (SavePath.isEmpty())return ;
            }
            if (QFile::exists(SavePath))
            {
                append++;
                QFile::copy("/tmp/voice",SavePath + QString::number(append));
            }
            QFile::copy("/tmp/voice",SavePath);
            m_ui->statusBar->showMessage("Saved!", 3000);
        }

    }
    else
    {
        m_ui->statusBar->showMessage("An error occured",2000);
        QMessageBox::critical(this ,"Error",error);
    }

}


void VoiceGen::on_engineCombobox_currentIndexChanged()
{
    // remove vioces from voice combobox
    auto voice_list_num = m_ui->voiceListCombobox->count();
    for (auto a=0; a < voice_list_num; a++)
    {
        m_ui->voiceListCombobox->removeItem(0);
    }
    //set new voice name for the specific engine
    std::array<QStringList,4> Voices = {{
        {"English (US)","English (UK)","Italian"}, // picotts
        {"English (US)","English (UK)","Italian"}, // picotts (voicegen)
        {" English (US)","Ivy","Joanna","Joey","Justin","Kendra","Kimberly","Matthew","Salli"," English (UK)","Amy","Brian","Emma"," English (AS)","Russell","Nicole"," English (WE)","Geraint"," English (IN)","Aditi","Raveena"},//Streamlabs
        {"En_us","En_uk"},
    }};
    if (m_ui->engineCombobox->currentIndex() == 2)m_ui->statusBar->showMessage("535 Character limit",3000);
    m_ui->voiceListCombobox->addItems(Voices[m_ui->engineCombobox->currentIndex()]);
}



void VoiceGen::on_actionRead_triggered()
{
    if (tts->isRunning())
    {
        tts->terminate();
    }
    UserTextInput =  m_ui->main_input->toPlainText();
    if (UserTextInput.isEmpty())
    {
        QMessageBox::warning(this ,"No Input","Please write something into the text field!");
    }
    else
    {
        tts->runTTS(m_ui->engineCombobox->currentIndex(),m_ui->voiceListCombobox->currentText(),UserTextInput,true);//in thread
        m_ui->statusBar->showMessage("Loading...");
        readsaveflag=read;
    }
}



void VoiceGen::on_actionStop_triggered()
{
    player->stop();
    m_ui->statusBar->showMessage("");
    if (tts->isRunning())
    {
        tts->terminate();
        m_ui->statusBar->showMessage("Process cancelled",2000);
    }
}

void VoiceGen::on_actionSave_As_triggered()
{
    if (m_ui->main_input->toPlainText().isEmpty())
    {
        QMessageBox::warning(this, "No Input", "Please write somthing into the text field");
        return;
    }
    m_ui->statusBar->showMessage("Getting data...",3000);
    readsaveflag=saveAs;
    tts->runTTS(m_ui->engineCombobox->currentIndex(),m_ui->voiceListCombobox->currentText(),m_ui->main_input->toPlainText(),false);
}


void VoiceGen::on_actionSave_triggered()
{
    if (m_ui->main_input->toPlainText().isEmpty())
    {
        QMessageBox::warning(this, "No Input", "Please write somthing into the text field");
        return;
    }
    m_ui->statusBar->showMessage("Getting data...",3000);
    readsaveflag=save;
    tts->runTTS(m_ui->engineCombobox->currentIndex(),m_ui->voiceListCombobox->currentText(),m_ui->main_input->toPlainText(),false);
}


void VoiceGen::on_actionAbout_triggered()
{
    QMessageBox::about(this, "About VoiceGen", "<p>VoiceGen is a simple piece of software for reading your texts.</p> <p>by PersianGolf</p> <a href=\"https://gitlab.com/PersianGolf/voicegen/-/tree/main#buy-me-a-coffee-\">Source code and Coffee☕</a> <p>This project is licensed under the <a href='https://gitlab.com/PersianGolf/voicegen/-/blob/main/LICENCE.md'>GNU GPL v3.0</a>.</p>");
}

void VoiceGen::on_actionQuit_triggered()
{
    delete tts;
    delete player;
    this->close();
}

