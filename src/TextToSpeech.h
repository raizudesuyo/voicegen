#pragma once
#include <QtCore>
#include <QThread>
#include "StreamlabsCommunication.h"
#include <QMediaPlayer>
class TextToSpeech : public QThread{
Q_OBJECT
signals:
    void taskFinished(QString error="");
public:
    TextToSpeech(QMediaPlayer* p);

    void runTTS(const int& e, const QString& n, const QString& w, const bool& sf);
    void run();// this is for thread. start() function fires this

private:
    QMediaPlayer *player;
    int engineIndex;
    bool streamflag;
    QString VoiceName;
    QString whattosay;
    void downloadtotmp(const QString& link);
};
