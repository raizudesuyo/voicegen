#include "TextToSpeech.h"
#include <QMediaPlayer>
#include <QFile>
#include <iostream>


TextToSpeech::TextToSpeech(QMediaPlayer* p)
{
    player = p;
}


void TextToSpeech::runTTS(const int &e, const QString &n,const QString &w, const bool &sf)
{
    engineIndex = e;
    VoiceName= n;
    whattosay = w;
    streamflag = sf;
    start();
}


void TextToSpeech::run(void)
{
    whattosay.replace('\n',", ");
    switch(engineIndex)
    {
        case 2:{//Streamlabs
            if (VoiceName.contains("US"))VoiceName="Joanna";
            else if(VoiceName.contains("UK"))VoiceName="Amy";
            else if (VoiceName.contains("AS"))VoiceName="Russell";
            else if (VoiceName.contains("WE"))VoiceName="Geraint";
            else if (VoiceName.contains("IN"))VoiceName="Aditi";
            StreamlabsCommunication *streamlabs = new StreamlabsCommunication(VoiceName, whattosay);
            if (streamlabs->isSuccessful())
            {
                QString streamlabslink = streamlabs->parseLink();
                if (streamflag)
                {
                   player->setMedia(QUrl(streamlabslink));
                   player->play();
                }
                else
                {
                    downloadtotmp(streamlabslink);
                }
                emit taskFinished();
            }
            else
            {
                if (whattosay.size()>535)
                {
                    emit taskFinished("Too many characters!\nStreamlabs tts engine only allows you to use around 530 characters. Please use another engine.");
                }
                else emit taskFinished("Couldn't connet to the server.\n");
            }
            delete streamlabs;
            break;
        }

        case 3:{//Google
            //https://translate.google.com/translate_tts?ie=UTF-8&tl=en_Us&client=tw-ob&q=%22hihihihi%22
            QString GoogleApiLink="https://translate.google.com/translate_tts?ie=UTF-8&tl="+ VoiceName + "&client=tw-ob&q=" + whattosay;
            if (streamflag)
            {
                player->setMedia(QUrl(GoogleApiLink));
                player->play();
            }
            else
            {
                downloadtotmp(GoogleApiLink);
            }
            emit taskFinished("");
            break;
        }
        default:{//Linux = 0
            if (VoiceName.contains("US"))VoiceName="en-US";
            else if(VoiceName.contains("UK"))VoiceName="en-GB";
            else if(VoiceName.contains("It"))VoiceName="it-IT";
            whattosay.replace('\'',"\'\\\'\'");//   '\''

            //a workaround for svox
            if (QFile::exists("/tmp/voice")) QFile::remove("/tmp/voice");

            QString picocmd = "text=" + QString("\'") + whattosay + QString("\'") + QString(" && pico2wave -w=/tmp/voice.wav --lang=\'") + VoiceName + QString('\'') + (" \"${text}\"");
            system(picocmd.toStdString().c_str());

            if (engineIndex==1)//add enhacement
            {
                system("ffmpeg -nostats -loglevel 0 -y -i /tmp/voice.wav -af \"firequalizer=gain_entry='entry(0,-7);entry(250,-4);entry(1000,2);entry(4000,6);entry(16000,12)'\" /tmp/output.wav");
                QFile::remove("/tmp/voice.wav");
                QFile::rename("/tmp/output.wav","/tmp/voice");
            }
            else QFile::rename("/tmp/voice.wav","/tmp/voice");//workaround
            if (streamflag)
            {
                player->setMedia(QUrl::fromLocalFile("/tmp/voice"));
                player->play();
            }
            emit taskFinished("");
        }
    }
}



void TextToSpeech::downloadtotmp(const QString &link)
{
    std::string downloadCommand = ("wget \"" + link.toStdString() + '\"' + " -q -O /tmp/voice");
    system(downloadCommand.c_str());
}



