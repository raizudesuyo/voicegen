#pragma once
#include <QString>
#include <QDebug>
#include <iostream>

using namespace std;

class StreamlabsCommunication{
public:
    StreamlabsCommunication(const QString &Voice, QString Text);
    QString parseLink();
    bool isSuccessful();
private:
    QString commandReadString(const QString &cmd);
    QString serverResponse;
    bool SuccessCheck;
};
