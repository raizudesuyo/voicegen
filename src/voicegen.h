#ifndef VOICEGEN_H
#define VOICEGEN_H

#include <QMainWindow>
#include <QScopedPointer>
#include <QMediaPlayer>
#include "TextToSpeech.h"
#include <QShortcut>

enum ReadSaveFlag{
    read=1 , saveAs=2, save=3
};

namespace Ui {
class VoiceGen;
}

class VoiceGen : public QMainWindow
{
    Q_OBJECT

public:
    explicit VoiceGen(QWidget *parent = nullptr);
    ~VoiceGen() override;

private slots:
    void on_actionRead_triggered();
    void on_actionAbout_triggered();
    void on_actionStop_triggered();
    void on_actionQuit_triggered();
    void on_engineCombobox_currentIndexChanged();
    void on_actionSave_As_triggered();
    void on_actionSave_triggered();
public slots:
    void on_ttsthread_finished(QString error);
    void on_ctrlm_triggered ();

private:
    QScopedPointer<Ui::VoiceGen> m_ui;
    QString UserTextInput;
    QString SavePath;
    QMediaPlayer* player;
    int fileNameNumberAppend = 0;
    TextToSpeech* tts;
    ReadSaveFlag readsaveflag;//one means read
    QShortcut *menubarShortcut;
};


#endif // VOICEGEN_H
